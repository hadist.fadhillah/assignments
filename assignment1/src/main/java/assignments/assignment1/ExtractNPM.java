package assignments.assignment1;

/*
Nama: Hadist Fadhillah
NPM: 2006463490
Tanggal: 19-03-2021
Deskripsi: Program ini dibuat mengkuti template yang sudah diikuti. Untuk validasi
dilakukan dengan konsep me-return nilai true apabila lulus cek dari if-statement
yang disediakan. Program otomatis akan menjalankan method extract setelah
method validate mereturn nilai true.
*/
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ExtractNPM {
    /*
    You can add other method do help you solve
    this problem

    Some method you probably need like
    - Method to get tahun masuk or else
    - Method to help you do the validation
    - and so on
    */

    public static boolean validate(long npm) {
        // TODO: validate NPM, return it with boolean
        final int DIGIT_NPM = 14;
        final int TAHUN_KELAHIRAN_MAKS = 2006;
        final int TAHUN_MINIMAL_MASUK = 2010;
        final int TAHUN_SEKARANG = 2021;
        String strNPM = String.valueOf(npm);

        // Cek jumlah digit NPM
        if (String.valueOf(npm).length() != DIGIT_NPM) {
            return false;
        }

        // Cek umur
        String tahunKelahiranUser = strNPM.substring(9,13);
        if (Integer.parseInt(tahunKelahiranUser) > TAHUN_KELAHIRAN_MAKS) {
            return false;
        }

        // Bagian A: Tahun masuk
        String tahunMasukUser = strNPM.substring(0,2);
        if (Integer.parseInt(tahunMasukUser)+2000 < TAHUN_MINIMAL_MASUK) {
            return false;
        } else if (Integer.parseInt(tahunMasukUser) > TAHUN_SEKARANG) {
            return false;
        }

        // Bagian B: Nama jurusan
        String jurusanUser = strNPM.substring(2,4);
        String[] kelompokJurusan = {"01", "02", "03", "11", "12"};
        List<String> jurusanList = Arrays.asList(kelompokJurusan);
        boolean hasil = jurusanList.contains(jurusanUser);
        if(!hasil) {
            return false;
        }

        // Bagian E: Kode NPM
        int[] digitNPM = new int [14];
        for (int i = 0; i < 14; i++) {
            digitNPM[i] = strNPM.charAt(i) - '0';
        }
        int acc = 0;
        for (int i = 0, j = 12;i < 6; i++, j--) {
            int perkalian = digitNPM[i] * digitNPM[j];
            acc += perkalian;
        }

        boolean bagianE = true;
        while (bagianE) {
            if (acc >= 10) {
                String strAcc = String.valueOf(acc);
                acc = 0;
                for (int i = 0; i < strAcc.length(); i++){
                    char setiapAngka = strAcc.charAt(i);
                    int sementara = Integer.parseInt(String.valueOf(setiapAngka));
                    acc = acc + sementara;
                }
            } else {
                bagianE = false;
            }
        }
        int digitTerakhir = digitNPM[13];
        if (acc != digitTerakhir) {
            System.out.println("kucing");
            return false;
        }

        return true;
    }

    public static String extract(long npm) {
        // TODO: Extract information from NPM, return string with given format

        // Tahun masuk
        String strNPM = String.valueOf(npm);
        String tahunMasuk = "Tahun masuk: 20" + strNPM.substring(0,2);

        // Nama jurusan
        String kodeJurusan = strNPM.substring(2, 4);
        String namaJurusan = "";
        switch (kodeJurusan) {
            case "01": namaJurusan = "Ilmu Komputer"; break;
            case "02": namaJurusan = "Sistem Informasi"; break;
            case "03": namaJurusan = "Teknologi Informasi"; break;
            case "11": namaJurusan = "Teknik Telekomunikasi"; break;
            case "12": namaJurusan = "Teknik Elektro"; break;
        }

        // Tanggal lahir
        String hariLahir = strNPM.substring(4, 6);
        String bulanLahir = strNPM.substring(6, 8);
        String tahunLahir = strNPM.substring(8, 12);
        String tanggalLahir = "Tanggal Lahir: " + hariLahir + "-" + bulanLahir + "-" + tahunLahir;

        String hasil = tahunMasuk + "\n" + namaJurusan + "\n" + tanggalLahir;
        return hasil;
    }

    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            boolean hasil = validate(npm);
            if (hasil) {
                System.out.println(extract(npm));
                exitFlag = true;
            } else {
                System.out.println("NPM tidak Valid!");
            }

            // TODO: Check validate and extract NPM

        }
        input.close();
    }
}
